import React, { Component } from "react"
import { Tab, Tabs, TabList, TabPanel } from "react-tabs"
import Display from "./Display"

import {
  lastInStock,
  titleOfFirstBook,
  lessThanTen,
  averageDollarValue,
  sanitizeNames,
  availablePrices
} from "../exercises"
import { expected } from "../exercises/solutions"

import "react-tabs/style/react-tabs.css"

const exerciseTitles = [
  "lastInStock",
  "titleOfFirstBook",
  "lessThanTen",
  "averageDollarValue",
  "sanitizeNames",
  "availablePrices"
]

const exerciseFns = [
  lastInStock,
  titleOfFirstBook,
  lessThanTen,
  averageDollarValue,
  sanitizeNames,
  availablePrices
]

const exerciseDescs = [
  "This function should tell you if the last book in the collection is in stock.",
  "This function should give you the title of the first book in the collection.",
  "This function should show the a list of all dollar values less than 10.",
  "This function should give you the average dollar value of all the books in the collection.",
  'This function should return "sanitized" versions of the book titles.',
  "This function should return an array of formatted prices for all available books in the collection."
]

// This is not the way to do things, as these arrays could fall out of sync with one another, don't follow my example here
const exercises = exerciseTitles.map((e, i) => ({
  title: e,
  fn: exerciseFns[i],
  desc: exerciseDescs[i],
  expect: expected[i]
}))

export default () => (
  <Tabs>
    <h1>Exercises</h1>
    <TabList>{exercises.map(({ title }, i) => <Tab>{i + 1}</Tab>)}</TabList>

    {exercises.map(({ expect, title, fn, desc }) => (
      <TabPanel>
        <Display
          expect={expect}
          fn={fn}
          title={title}
          desc={desc}
          key={title}
        />
      </TabPanel>
    ))}
  </Tabs>
)
