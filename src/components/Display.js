import React, { Component, Fragment } from "react"
import styled from "styled-components"
import { BOOKS } from "../exercises"

const Button = styled.input`
  border-radius: 6px;
  padding: 0.5rem;
  background-color: #2980b9;
  color: white;
  border: 1px solid white;
  &:hover {
    color: #2980b9;
    background-color: white;
    border: 1px solid #2980b9;
  }
  font-size: 0.8rem;
`

const ResultDisplay = styled.pre`
  padding: 1rem;
  background-color: ${({ correct }) => (correct ? "#2ecc71" : "#f39c12")};
  border-radius: 6px;
  white-space: pre-wrap;
  color: white;
`

const ErrorDisplay = ResultDisplay.extend`
  background-color: #e74c3c;
  color: white;
`

const ExpectDisplay = ResultDisplay.extend`
  background-color: whitesmoke;
  color: black;
`

export default class Display extends Component {
  state = {
    results: null,
    error: null
  }
  componentDidCatch(error, info) {
    this.setState(() => ({ hasError: true }))
  }
  render() {
    const { title, fn, desc, expect } = this.props
    const { error, results } = this.state
    return (
      <Fragment>
        <h2>{title}</h2>
        <p>{desc}</p>
        <Button
          type="button"
          value={`Run ${title}(BOOKS)`}
          onClick={e => {
            e.preventDefault()
            // This is not the functional way(TM), TODO: re-factor with a Maybe
            try {
              fn(BOOKS)
              this.setState(() => ({ results: fn(BOOKS) }))
            } catch (error) {
              this.setState(() => ({ error }))
            }
          }}
        />
        {results !== null && (
          <Fragment>
            <p>Expected:</p>
            <ExpectDisplay>{JSON.stringify(expect, null, 2)}</ExpectDisplay>
            <p>Received:</p>
            <ResultDisplay
              correct={JSON.stringify(results) === JSON.stringify(expect)}
            >
              {JSON.stringify(results, null, 2)}
            </ResultDisplay>
          </Fragment>
        )}
        {error && (
          <ErrorDisplay>
            Something went wrong. Did you remember to implement the function
            (and save the file)?
          </ErrorDisplay>
        )}
      </Fragment>
    )
  }
}
