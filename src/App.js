import React, { Component, Fragment } from "react"
import styled from "styled-components"
import Exercises from "./components/Exercises"
import { BOOKS } from "./exercises"

const importAll = r => r.keys().map(r)
const covers = importAll(require.context("./covers", false, /\.jpe?g$/))

const Main = styled.main`
  background-color: white;
  display: grid;
  padding: 1rem;
  grid-template-columns: repeat(3, 1fr);
  grid-template-rows: repeat(3, 1fr);
  grid-gap: 1rem;
`
const Aside = styled.aside`
  background-color: white;
  box-shadow: 1.5rem -1.5rem 1.5rem 1.5rem #00000030;
  padding: 1rem;
  max-height: 100vh;
  overflow: scroll;
`

const Card = styled.section`
  position: relative;
  background-color: transparent;
  background-size: cover;
  background-image: url("${({ bg }) => bg}");
  background-repeat: no-repeat;
  background-position: center;
`
const BookInfo = styled.div`
  background: #ecf0f150;
  height: 100%;
  display: grid;
  align-content: flex-end;
  & div {
    background: white;
    padding: 0.2rem;
    margin: 0;
    & div,
    h4 {
      padding: 0;
      margin: 0;
    }
  }
  &:hover {
    background: transparent;
    color: white;
    & div {
      background: #2c3e50d0;
    }
  }
`

const SoldOut = styled.div`
  position: absolute;
  top: 2rem;
  color: white;
  font-size: 0.8rem;
  font-weight: 800;
  text-align: center;
  background: #e74c3c;
  transform: rotate(-10deg);
  transform-origin: top left;
  box-shadow: 0.25rem 0.25rem 0.25rem #000000c0;
  padding: 0.2rem 0.8rem 0.2rem 0.8rem;
`

class App extends Component {
  componentDidMount() {
    console.log(covers)
  }
  render() {
    return (
      <Fragment>
        <Main>
          {BOOKS.map(({ title, author, dollarValue, inStock, iSBN }) => (
            <Fragment>
              <Card bg={covers.find(e => e.includes(iSBN))}>
                <BookInfo>
                  <div>
                    <h4>{title}</h4>
                    <div>by {author}</div>
                    <div>(${dollarValue})</div>
                  </div>
                </BookInfo>
                {!inStock && <SoldOut>OUT OF STOCK!</SoldOut>}
              </Card>
            </Fragment>
          ))}
        </Main>
        <Aside>
          <Exercises />
        </Aside>
      </Fragment>
    )
  }
}

export default App
