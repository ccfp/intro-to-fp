// This is the solutions file!
// Don't look in here unless you absolutely have to!

const {
  BOOKS,
  compose,
  map,
  filter,
  reduce,
  prop,
  first,
  last
} = require("./index")

// Exercise 1:
// This function should tell you if the last book in the collection is in stock.
// lastInStock :: [Book] -> Boolean
const lastInStock = compose(
  prop("inStock"),
  last
)

// Exercise 2:
// This function should give you the title of the first book in the collection.
// titleOfFirstBook :: [Book] -> String
const titleOfFirstBook = compose(
  prop("title"),
  first
)

// Exercise 3:
// This function should show all prices of books with a dollar value less than 10.
// lessThanTen :: [Book] -> [Number]
const lessThanTen = compose(
  filter(x => x < 10),
  map(prop("dollarValue"))
)

// Exercise 4:
// This function should give you the average dollar value of all the books in the collection.
// Make use of the following function:
const average = xs => reduce((x, y) => x + y)(0)(xs) / xs.length
// averageDollarValue :: [Book] -> Number
const averageDollarValue = compose(
  average,
  map(prop("dollarValue"))
)

// Exercise 5:
// This function should return "sanitized" versions of the book titles.
// All letters should be lower-cased and all spaces replaced with "_".
// E.g., "The Great Gatsby" would be sanitized as "the_great_gatsby".
// Make use of the following functions:
const replaceAll = x => y => str => str.replace(new RegExp(x, "g"), y)
const toLower = str => str.toLowerCase()
// sanitizeNames :: [Book] -> [String]
const sanitizeNames = compose(
  map(replaceAll(" ")("_")),
  map(toLower),
  map(prop("title"))
)

// Exercise 6:
// This function should return an array of formatted prices for all available books in the collection.
// Use the following functions
const formatPrice = num =>
  num.toLocaleString("en-US", { style: "currency", currency: "USD" })
const join = str => arr => arr.join(str)
// availablePrices :: [Book] -> String
const availablePrices = compose(
  join(", "),
  map(formatPrice),
  map(prop("dollarValue")),
  filter(prop("inStock"))
)

// This is super-hacky
const expected = [
  true,
  "Things Fall Apart",
  [9, 8, 9, 7],
  11,
  [
    "things_fall_apart",
    "saragossa_manuscript",
    "the_bell_jar",
    "ficciones",
    "frankenstein",
    "a_clockwork_orange",
    "sons_and_lovers",
    "blood_meridian",
    "seven_gothic_tales"
  ],
  "$9.00, $13.00, $9.00, $7.00, $11.00, $12.00"
]

module.exports = {
  BOOKS,
  lastInStock,
  titleOfFirstBook,
  lessThanTen,
  averageDollarValue,
  sanitizeNames,
  availablePrices,
  compose,
  map,
  filter,
  reduce,
  prop,
  first,
  last,
  expected
}
