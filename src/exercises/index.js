// These examples are heavily borrowed/stolen from Brian Lonsdorf's
// Mostly Adequate Guide to Functional Programming
// https://drboolean.gitbooks.io/mostly-adequate-guide/ch5.html#in-summary

// Compose, our most important function
const compose = (...fns) => x => fns.reduceRight((val, fn) => fn(val), x)

// Useful curried functions
const map = fn => arr => arr.map(fn)
const filter = fn => arr => arr.filter(fn)
const reduce = fn => acc => arr => arr.reduce(fn, acc)
const prop = key => obj => obj[key]
const first = arr => arr[0]
const last = arr => arr[arr.length - 1]

// Some Books
const BOOKS = [
  {
    title: "Things Fall Apart",
    author: "Chinua Achebe",
    dollarValue: 9,
    inStock: true,
    iSBN: 9782708701915
  },
  {
    title: "Saragossa Manuscript",
    author: "Jan Potocki",
    dollarValue: 8,
    inStock: false,
    iSBN: 9782081211438
  },
  {
    title: "The Bell Jar",
    author: "Sylvia Plath",
    dollarValue: 13,
    inStock: true,
    iSBN: 9781632451842
  },
  {
    title: "Ficciones",
    author: "Jorge Luis Borges",
    dollarValue: 9,
    inStock: true,
    iSBN: 9788806139889
  },
  {
    title: "Frankenstein",
    author: "Mary Shelley",
    dollarValue: 10,
    inStock: false,
    iSBN: 9781977841438
  },
  {
    title: "A Clockwork Orange",
    author: "Anthony Burgess",
    dollarValue: 7,
    inStock: true,
    iSBN: 9781858496344
  },
  {
    title: "Sons and Lovers",
    author: "D. H. Lawrence",
    dollarValue: 20,
    inStock: false,
    iSBN: 9781847022479
  },
  {
    title: "Blood Meridian",
    author: "Cormac McCarthy",
    dollarValue: 11,
    inStock: true,
    iSBN: 9788497939003
  },
  {
    title: "Seven Gothic Tales",
    author: "Isak Dinesen",
    dollarValue: 12,
    inStock: true,
    iSBN: 9788702063097
  }
]

// Exercise 1:
// This function should tell you if the last book in the collection is in stock.
// lastInStock :: [Book] -> Boolean
const lastInStock = undefined

// Exercise 2:
// This function should give you the title of the first book in the collection.
// titleOfFirstBook :: [Book] -> String
const titleOfFirstBook = undefined

// Exercise 3:
// This function should show all prices of books with a dollar value less than 10.
// lessThanTen :: [Book] -> [Number]
const lessThanTen = undefined

// Exercise 4:
// This function should give you the average dollar value of all the books in the collection.
// Make use of the following function:
const average = xs => reduce((x, y) => x + y)(0)(xs) / xs.length
// averageDollarValue :: [Book] -> Number
const averageDollarValue = undefined

// Exercise 5:
// This function should return "sanitized" versions of the book titles.
// All letters should be lower-cased and all spaces replaced with "_".
// E.g., "The Great Gatsby" would be sanitized as "the_great_gatsby".
// Make use of the following functions:
const replaceAll = x => y => str => str.replace(new RegExp(x, "g"), y)
const toLower = str => str.toLowerCase()
// sanitizeNames :: [Book] -> [String]
const sanitizeNames = undefined

// Exercise 6:
// This function should return a string that lists the formatted prices for all available books in the collection.
// Use the following functions
const formatPrice = num =>
  num.toLocaleString("en-US", { style: "currency", currency: "USD" })
const join = str => arr => arr.join(str)
// availablePrices :: [Book] -> String
const availablePrices = undefined

module.exports = {
  BOOKS,
  lastInStock,
  titleOfFirstBook,
  lessThanTen,
  averageDollarValue,
  sanitizeNames,
  availablePrices,
  compose,
  map,
  filter,
  reduce,
  prop,
  first,
  last
}
