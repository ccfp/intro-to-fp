const {
  BOOKS,
  lastInStock,
  titleOfFirstBook,
  lessThanTen,
  averageDollarValue,
  sanitizeNames,
  availablePrices
} = require("../exercises")

test("lastInStock", () => {
  expect(lastInStock(BOOKS)).toEqual(true)
})

test("titleOfFirstBook", () => {
  expect(titleOfFirstBook(BOOKS)).toEqual("Things Fall Apart")
})

test("lessThanTen", () => {
  const expected = [9, 8, 9, 7]

  expect(lessThanTen(BOOKS)).toEqual(expected)
})

test("averageDollarValue", () => {
  expect(averageDollarValue(BOOKS)).toEqual(11)
})

test("sanitizeNames", () => {
  const expected = [
    "things_fall_apart",
    "saragossa_manuscript",
    "the_bell_jar",
    "ficciones",
    "frankenstein",
    "a_clockwork_orange",
    "sons_and_lovers",
    "blood_meridian",
    "seven_gothic_tales"
  ]

  expect(sanitizeNames(BOOKS)).toEqual(expected)
})

test("availablePrices", () => {
  const expected = "$9.00, $13.00, $9.00, $7.00, $11.00, $12.00"

  expect(availablePrices(BOOKS)).toEqual(expected)
})
