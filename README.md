# Intro to Functional Programming
---
## Composition Exercises

These exercises are largely borrowed from Kyle Shevlin's ["Just Enough Functional Programming to be a Danger to Yourself and Coworkers"](https://www.youtube.com/watch?v=-4QNj7TJjgo) (slides [here](http://slides.com/kyleshevlin/just-enough-fp#/), original git repo of exercises [here](https://github.com/kyleshevlin/fp-composition-exercises)).
Kyle had in turn based his exercises off of Brian Lonsdorf's [Mostly Adequate Guide to Functional Programming](https://mostly-adequate.gitbooks.io/mostly-adequate-guide/ch05.html#in-summary) (covering material from the first five chapters of that book), with a view to simplifying and making them a bit easier or beginner-friendly.
This app tries to add to that effort by giving the exercises a visual counterpart.

## To get started
* Clone this repo.
  ```bash
  git clone git@gitlab.com:ccfp/intro-to-fp.git
  ```

* Install [Node](http://nodejs.org/) if you haven't already.

* Install modules (this will take a minute) with `npm install`

* Open `src/exercises/index.js` in your favorite editor and use `compose` to implement the functions for the six exercises listed there.

* Run `npm run test` to test, and/or open the app in your browser with `npm run start`

(This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).)